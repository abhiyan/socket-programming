

#include <sys/socket.h>       /*  socket definitions        */
#include <sys/types.h>        /*  socket types              */
#include <arpa/inet.h>        /*  inet (3) funtions         */
#include <unistd.h>           /*  misc. UNIX functions      */

#include "helper.h"           /*  Our own helper functions  */
#include "helper.c"
#include <stdlib.h>
#include <string.h>
#include <stdio.h>


/*  Global constants  */

#define MAX_LINE           (1000)


/*  Function declarations  */

int ParseCmdLine(int argc, char *argv[], char **szAddress, char **szPort);


/*  main()  */

int main(int argc, char *argv[]) {

    int       conn_s;                /*  connection socket         */
    short int port;                  /*  port number               */
    struct    sockaddr_in servaddr;  /*  socket address structure  */
    char      buffer[MAX_LINE];      /*  character buffer          */
    char     *szAddress;             /*  Holds remote IP address   */
    char     *szPort;                /*  Holds remote port         */
    char     *endptr;                /*  for strtol()              */


    /*  Get command line arguments  */

    ParseCmdLine(argc, argv, &szAddress, &szPort);


    /*  Set the remote port  */

    port = strtol(szPort, &endptr, 0);
    if ( *endptr ) {
	printf("ECHOCLNT: Invalid port supplied.\n");
	exit(EXIT_FAILURE);
    }
	

    /*  Create the listening socket  */

    if ( (conn_s = socket(AF_INET, SOCK_STREAM, 0)) < 0 ) {
	fprintf(stderr, "ECHOCLNT: Error creating listening socket.\n");
	exit(EXIT_FAILURE);
    }


    /*  Set all bytes in socket address structure to
        zero, and fill in the relevant data members   */

    memset(&servaddr, 0, sizeof(servaddr));
    servaddr.sin_family      = AF_INET;
    servaddr.sin_port        = htons(port);


    /*  Set the remote IP address  */

    if ( inet_aton(szAddress, &servaddr.sin_addr) <= 0 ) {
	printf("ECHOCLNT: Invalid remote IP address.\n");
	exit(EXIT_FAILURE);
    }

    
    /*  connect() to the remote echo server  */

    if ( connect(conn_s, (struct sockaddr *) &servaddr, sizeof(servaddr) ) < 0 ) {
	printf("ECHOCLNT: Error calling connect()\n");
	exit(EXIT_FAILURE);
    }


    /*  Get string to echo from user  */

    printf("Send to server: ");
    char name_of_file[32];
    /*taking in input and storing into char*/
    char first_inp;
    char sec_inp;
    do {
        first_inp = getchar();
        sec_inp = getchar();    
    } while((first_inp != 's') && (first_inp != 't') && (first_inp != 'q'));    //checking initial input
    
    if (first_inp == 'q'){
        return 0;
    }else if(first_inp == 's'){
        char c;
        int i = 0;
        /*taking characters one by one to make sure \n is included at the end of the string*/
        while ((c = getchar()) != '\n'){
            buffer[i++] = c;
        }
        buffer[i] = '\0';

    /*concatenation of CAP\\n with string input and load into buffer*/
        char dest[32], src[12];
        strcpy(src, "\\n");
        strcpy(dest, "CAP\\n");
        strcat(dest, buffer);
        strcat(dest, src);
        strcpy(buffer, dest);
        buffer[strlen(buffer)] = '\n';
    }else if(first_inp == 't'){
        char c;
        int i = 0;
        /*taking characters one by one to make sure \n is included at the end of the string*/
        while ((c = getchar()) != '\n'){
            buffer[i++] = c;
        }
        buffer[i] = '\0';
        
        strcpy(name_of_file, buffer);
    /*concatenation of FILE\\n with string input and load into buffer*/
        char dest[32], src[12];
        strcpy(src, "\\n");
        strcpy(dest, "FILE\\n");
        strcat(dest, name_of_file);
        //printf("%s\n", dest);
        strcat(dest, src);
        strcpy(buffer, dest);
        buffer[strlen(buffer)] = '\n';
        //printf("%s\n", buffer);
        

    }
    

    /*  Send string to server, and retrieve response  */

    Writeline(conn_s, buffer, strlen(buffer));
    Readline(conn_s, buffer, MAX_LINE-1);

    if(first_inp == 't'){
        char size_of_file[64];
        char b;
        int i = 0;
        while ((buffer[i]) != '\n'){
            size_of_file[i++] = b;
        }
        size_of_file[i] = '\0';
        FILE *writefile;
        for (int j = 0; j < strlen(buffer) - strlen(size_of_file)-2; j++){
            buffer[i] = buffer[strlen(size_of_file)+2+i];
        }
        buffer[strlen(buffer)] = '\0';
        writefile = fopen(name_of_file, "wb");
        if (writefile){
            fwrite(buffer, sizeof(buffer), 1, writefile);
        }}
        
      /*Output echoed string  */

    printf("Echo response: %s\n", buffer);

    return EXIT_SUCCESS;
}


int ParseCmdLine(int argc, char *argv[], char **szAddress, char **szPort) {

	*szAddress = argv[1];
	*szPort = argv[2];

    return 0;
}
